﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzFactory : IFizzBuzzFactory
    {
        private IEnumerable<IFizzBuzzRule> fizzBuzzRule;

        public FizzBuzzFactory(IList<IFizzBuzzRule> fizzBuzzRule)
        {
            this.fizzBuzzRule = fizzBuzzRule;
        }

        public IEnumerable<string> GetFizzBuzzResult(int number)
        {
            List<string> fizzBuzzList = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                string output = i.ToString();

                var result = this.fizzBuzzRule.FirstOrDefault(a => a.IsValid(i));

                if (result != null)
                {
                    output = result.DispalyFizzOrBuzz();
                }
                fizzBuzzList.Add(output);
            }
            return fizzBuzzList;
        }
    }
}