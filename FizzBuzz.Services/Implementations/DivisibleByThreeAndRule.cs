﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interfaces;
    using System;

    public class DivisibleByThreeAndRule : IFizzBuzzRule
    {
        private IDayProvider dayProvider;

        public DivisibleByThreeAndRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return ((number % 3 == 0) && (number % 5 == 0));
        }

        public string DispalyFizzOrBuzz()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? "buzz wuzz" : "fizz buzz";
        }
    }
}
