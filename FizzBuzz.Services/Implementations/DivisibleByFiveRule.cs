﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interfaces;
    using System;

    public class DivisibleByFiveRule : IFizzBuzzRule
    {
        private IDayProvider dayProvider;

        public DivisibleByFiveRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return number % 5 == 0;
        }

        public string DispalyFizzOrBuzz()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? "wuzz" : "buzz";
        }
    }
}
