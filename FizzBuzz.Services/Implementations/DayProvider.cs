﻿namespace FizzBuzz.Services.Implementations
{
    using FizzBuzz.Services.Interfaces;
    using System;

    public class DayProvider : IDayProvider
    {
        public bool IsValid(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
