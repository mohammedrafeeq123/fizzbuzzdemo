﻿namespace FizzBuzz.Services.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzFactory
    {
        IEnumerable<string> GetFizzBuzzResult(int number);
    }
}
