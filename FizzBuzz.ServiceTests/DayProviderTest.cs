﻿namespace BusinessLogic
{
    using FizzBuzz.Services.Implementations;
    using Moq;
    using NUnit.Framework;
    using System;
    using FizzBuzz.Services.Interfaces;

    [TestFixture]
    public class DayProviderTests
    {
        [TestCase(true, DayOfWeek.Wednesday)]
        [TestCase(false, DayOfWeek.Thursday)]
        [TestCase(false,DayOfWeek.Tuesday)]
        public void DayOfWeekTest(bool day, DayOfWeek dayCheck)
        {      
            var dateProvide = new DayProvider();
            var actualResult = dateProvide.IsValid(dayCheck);
            Assert.AreEqual(actualResult, day);
        }
    }
}
//