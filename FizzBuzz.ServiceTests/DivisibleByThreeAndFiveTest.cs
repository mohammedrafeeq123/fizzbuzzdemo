﻿using System;

namespace BusinessLogic
{
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivisibleByThreeAndFiveTests
    {
        private Mock<IDayProvider> dayProvider;

        [SetUp]
        public void Setup()
        {
            this.dayProvider = new Mock<IDayProvider>();
        }

        [TestCase(15, true)]
        [TestCase(3, false)]
        [TestCase(30, true)]
        public void IsValidTest(int value, bool expectedResult)
        {
            var result = new DivisibleByThreeAndRule(this.dayProvider.Object);
            var actualResult = result.IsValid(value);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestCase(false, "fizz buzz")]
        [TestCase(true, "buzz wuzz")]
        public void TestForFizzBuzzOrBuzzWuzz(bool day, string expectedResult)
        {
            var values = this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(day);
            var result = new DivisibleByThreeAndRule(this.dayProvider.Object);
            var actualResult = result.DispalyFizzOrBuzz();
            Assert.AreEqual(expectedResult, actualResult);
        }      
    }
}
