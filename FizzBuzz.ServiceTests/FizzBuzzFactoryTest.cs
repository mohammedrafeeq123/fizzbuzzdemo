﻿namespace BusinessLogic
{
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzFactoryTest
    {          
        private Mock<IFizzBuzzRule> fizzBuzzRule;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
        }

        [TestCase(0, ( new string[] { }))]
        public void FizzBuzzTestForZero(int value, string[] expected)
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(false);
            this.fizzBuzzRule.Setup(y => y.DispalyFizzOrBuzz()).Returns(string.Empty);
            FizzBuzzFactory factory = new FizzBuzzFactory(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var actual = factory.GetFizzBuzzResult(value);
            Assert.AreEqual(actual, expected);
        }
        [TestCase(2, new string[] {"1","2"})]
        public void FizzBuzzTestForOne(int value, string[] expected)
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(false);
            this.fizzBuzzRule.Setup(y => y.DispalyFizzOrBuzz()).Returns(string.Empty);
            FizzBuzzFactory factory = new FizzBuzzFactory(new List<IFizzBuzzRule>() {this.fizzBuzzRule.Object});
            var result = factory.GetFizzBuzzResult(value);
            Assert.AreEqual(result, expected);
        }

        [TestCase(4, "fizz")]
        public void ThreeDivisionTest(int value, string expected)
        {
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(true);
            this.fizzBuzzRule.Setup(y => y.DispalyFizzOrBuzz()).Returns("fizz");
            FizzBuzzFactory factory = new FizzBuzzFactory(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = factory.GetFizzBuzzResult(value);
            var actual = result.ElementAt(2);

             Assert.AreEqual(actual, expected);
        }

        [TestCase(5, "buzz")]
        public void FiveDivisionTest(int value, string expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(true);
            this.fizzBuzzRule.Setup(y => y.DispalyFizzOrBuzz()).Returns("buzz");
            FizzBuzzFactory factory= new FizzBuzzFactory(new List<IFizzBuzzRule>(){this.fizzBuzzRule.Object});
            var result = factory.GetFizzBuzzResult(value);
            var actual = result.ElementAt(4);
            Assert.AreEqual(actual, expected);
        }

        [TestCase(15, "fizz buzz")]
        public void ThreeAndFiveDivisionTest(int value, string expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(true);
            this.fizzBuzzRule.Setup(y => y.DispalyFizzOrBuzz()).Returns(expected);
            FizzBuzzFactory factory = new FizzBuzzFactory(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = factory.GetFizzBuzzResult(value);
            var actual = result.ElementAt(14);
            Assert.AreEqual(actual, expected);
        }
    }
}
