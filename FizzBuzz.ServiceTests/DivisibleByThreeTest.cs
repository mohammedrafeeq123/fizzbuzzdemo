﻿using System;

namespace BusinessLogic
{

    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
     public class DivisibleByThreeTests
     {
         private Mock<IDayProvider> dayProvider;

         [SetUp]
         public void Setup()
         {
             this.dayProvider = new Mock<IDayProvider>();
         }

         [TestCase(1, false)]
         [TestCase(3, true)]
         [TestCase(5, false)]
         public void IsValidTest(int value, bool expectedResult)
         {
             var result = new DivisibleByThreeRule(this.dayProvider.Object);
             var actualResult = result.IsValid(value);          
             Assert.AreEqual(actualResult, expectedResult);
         }

         [TestCase(false, "fizz")]
         [TestCase(true, "wizz")]
         public void TestForfizzOrWizz(bool day, string expectedResult)
         {
            var setUpValue = this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(day);
            var result = new DivisibleByThreeRule(this.dayProvider.Object);
            var actualResult = result.DispalyFizzOrBuzz();
            Assert.AreEqual(expectedResult, actualResult);
         }
    }
}