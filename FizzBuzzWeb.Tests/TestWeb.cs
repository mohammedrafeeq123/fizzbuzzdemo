﻿namespace FizzBuzzWebTest
{
    using FizzBuzz.Services.Interfaces;
    using FizzBuzzWeb.Controllers;
    using FizzBuzzWeb.Models;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [TestFixture]
    public class TestWeb
    {
        private Mock<IFizzBuzzFactory> fizzBuzz;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzz = new Mock<IFizzBuzzFactory>();
        }

        [Test]
        public void GetIndexTest()
        {
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var result = controller.Index(new FizzBuzzModel()) as ViewResult;
            var actualResult = result.ViewName;
        
            Assert.AreEqual("Index", actualResult);
        }

        [TestCase(5, ( new string[] { "1", "2", "fizz", "4", "buzz"}))]       
        public void PostIndexTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetFizzBuzzResult(It.IsAny<int>())).Returns(expectedResult);
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var result = controller.Index(new FizzBuzzModel(){ Input = value }) as ViewResult;          
            var model = result.Model as FizzBuzzModel;         
            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(5, ( new string[] { "1", "2", "Fizz", "4", "Buzz" }))]
        [TestCase(25, ( new string[] { "Fizz", "22", "23", "Fizz", "Buzz" }))]
        public void NavigateTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetFizzBuzzResult(It.IsAny<int>())).Returns(expectedResult);
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var output = controller.Navigate(new FizzBuzzModel() { Input = value }, 1) as ViewResult;
            var model = output.Model as FizzBuzzModel;
            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(-1, (new string[]{"Number Should be Between 1 to 1000"}))]
        public void ModelTest(int value, string[] expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetFizzBuzzResult(It.IsAny<int>())).Returns(expectedResult);
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var output = controller.Index(new FizzBuzzModel() { Input = value }) as ViewResult;
            var model = output.Model as FizzBuzzModel;
            var result = model.FizzBuzzNumbers;
            var actualResult = result;
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}