﻿
namespace FizzBuzzWeb.Controllers
{
    using FizzBuzzWeb.Models;
    using System.Web.Mvc;
    using PagedList;
    using FizzBuzz.Services.Interfaces;

    public class FizzBuzzController : Controller
    {
        private int pageSize = 20;
        private IFizzBuzzFactory fizzBuzz;

        public FizzBuzzController(IFizzBuzzFactory fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View(new FizzBuzzModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FizzBuzzModel model)
        {
            model.FizzBuzzNumbers = this.fizzBuzz.GetFizzBuzzResult(model.Input).ToPagedList(1, pageSize);
            return View("Index", model);

        }

        public ActionResult Navigate(FizzBuzzModel model, int page)
        {
            model.FizzBuzzNumbers = this.fizzBuzz.GetFizzBuzzResult(model.Input).ToPagedList(1, pageSize);
            return View("Index", model); ;
        }
    }
}